# docker file build for register in docker hub
FROM php:7.4-fpm
MAINTAINER DucDo

ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get install -y \
	openssl zip unzip git \
	apt-utils gnupg libpng-dev libpq-dev \
	libfreetype6-dev \
    libjpeg62-turbo-dev \
# Install DB mysql
	&& docker-php-ext-install pdo_mysql pdo pdo_pgsql gd mysqli pgsql

#RUN pecl install redis-5.1.1 \
#	&& pecl install xdebug-2.8.1 \
#    && docker-php-ext-enable redis xdebug

#change setting php.ini ENV (/usr/local/etc/php)
#COPY ./custom_config.ini $PHP_INI_DIR/conf.d/custom_config.ini

RUN mv $PHP_INI_DIR/php.ini-production $PHP_INI_DIR/php.ini

WORKDIR /var/www/html

# Install Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer \
