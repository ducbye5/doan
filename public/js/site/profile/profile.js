/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 3);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/site/profile/profile.js":
/*!**********************************************!*\
  !*** ./resources/js/site/profile/profile.js ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

$(document).ready(function () {
  var country = $('#profile_country');
  var city = $('#profile_city');
  var district = $('#profile_district');
  var ward = $('#profile_ward');
  var street = $('#profile_street');
  var project = $('#profile_project');
  var address = $('#profile_address');
  var country_text = '';
  var city_text = '';
  var district_text = '';
  var ward_text = '';
  var street_text = '';
  var project_text = '';

  function AddAddress() {
    $(country).change(function () {
      country_text = country.val();
    });
    $(city).change(function () {
      if (city.val() != '') {
        city_text = city.val() + " - ";
      } else {
        city_text = city.val();
      }
    });
    $(district).change(function () {
      if (district.val() != '') {
        district_text = district.val() + " - ";
      } else {
        district_text = district.val();
      }
    });
    $(ward).change(function () {
      if (ward.val() != '') {
        ward_text = ward.val() + " - ";
      } else {
        ward_text = ward.val();
      }
    });
    $(street).change(function () {
      if (street.val() != '') {
        street_text = street.val() + " - ";
      } else {
        street_text = street.val();
      }
    });
    $(project).change(function () {
      if (project.val() != '') {
        project_text = project.val() + " - ";
      } else {
        project_text = project.val();
      }
    });
    var address_val = project_text + street_text + ward_text + district_text + city_text + country_text;
    address.val(address_val);
    address.text(address_val);
  }

  $('#select_profile_address').click(AddAddress);
  AddAddress();
});

/***/ }),

/***/ 3:
/*!****************************************************!*\
  !*** multi ./resources/js/site/profile/profile.js ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! F:\project\laravel\doan\resources\js\site\profile\profile.js */"./resources/js/site/profile/profile.js");


/***/ })

/******/ });