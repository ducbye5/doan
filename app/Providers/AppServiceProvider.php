<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $base_path = base_path();
        $folder = $base_path.'/app/Repositories';
        if (file_exists($folder)) {
            foreach (glob(app_path('Repositories/*.php')) as $path) {
                $file = pathinfo($path);
                $this->app->bind(
                    'App\Repositories\Interfaces\\' . $file['filename'] . 'Interface',
                    'App\Repositories\\' . $file['filename']
                );
            }
        }
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        \Schema::defaultStringLength(191);
    }
}
