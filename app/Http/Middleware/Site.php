<?php

namespace App\Http\Middleware;

use Closure;

use App\Services\HomeService;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Cache;

class Site
{

    private $homeService;

    public function __construct(
        HomeService $HomeService
    ) {
        $this->homeService = $HomeService;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Cache::pull('Information_Of_Company');dd('oke');
        $Infomation_Of_Company = Cache::get('Information_Of_Company');
        if (empty($Infomation_Of_Company)) {
            $Infomation_Of_Company = $this->homeService->getInformationOfCompany();
            Cache::put('Information_Of_Company', $Infomation_Of_Company,1);
        }
//
        \View::share('company',$Infomation_Of_Company);
        return $next($request);
    }
}
