<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Services\HomeService;

class HomeController extends Controller
{
    private $homeService;
    public function __construct(HomeService $HomeService){
        $this->homeService = $HomeService;
    }

    public function index()
    {
         $data = $this->homeService->index();
         return view('index',['list_data' => $data]);
    }

    public function indexMap()
    {
        return view('home');
    }

    public function searchProperties()
    {
        return view('home');
    }
}
