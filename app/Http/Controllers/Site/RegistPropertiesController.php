<?php

namespace App\Http\Controllers\Site;

use App\Http\Requests\RegistPropertiesRequest;
use App\Http\Requests\RegistUserRequest;
use App\Services\HelpService;
use App\Services\RegistPropertiesService;
use App\Http\Controllers\Controller;

class RegistPropertiesController extends Controller
{
//    private $helpService;
    private $registPropertiesService;
    public function __construct(
        HelpService $HelpService,
        RegistPropertiesService $RegistPropertiesService
    ){
        $this->helpService = $HelpService;
        $this->registPropertiesService = $RegistPropertiesService;
    }

    public function index()
    {
        $data = $this->registPropertiesService->index();
        $view = 'component.RegistProperties';
    	return view($view);
    }

    public function regist(PostNewsRequest $request)
    {
        $request = $request->all();dd($request);
        $result_regist = $this->registPropertiesService->regist($request);
        $this->put_notification_of_regist_property($result_regist['status'],$result_regist['message']);
        return redirect()->back();
    }

    private function put_notification_of_regist_property($status = false,$message = null){
        if($status){
            \Log::debug('__REGIST_NEWS_IS_SUCCESS__');
        }else{
            \Log::debug('__REGIST_NEWS_IS_FALSE__');
        }
        if($message == null){
            \Session::flash('post_news_noti',__('PostNews.regist_fail'));
        }else{
            \Session::flash('post_news_noti',$message);
        }
    }
}
