<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PropertiesForRentalController extends Controller
{
    public function __construct(){

    }

    public function index()
    {
    	return view('component.SellAndRental');
    }

    public function getList_Dwelling_house()
    {
    	return view('component.SellAndRental');
    }

    public function getList_Apartment()
    {
    	return view('component.SellAndRental');
    }

    public function getList_Land()
    {
    	return view('component.SellAndRental');
    }

    public function getList_Other_real_estate()
    {
    	return view('component.SellAndRental');
    }
}
