<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\UserService;
use App\Http\Requests\LoginCustomerRequest;
use App\Http\Requests\RegistCustomerRequest;

class CustomerController extends Controller
{
	private $userService;
    public function __construct(
        UserService $UserService
    ){
    	$this->userService = $UserService;
    }

    public function login(LoginCustomerRequest $request)
    {
    	$input['username'] = $request->username;
    	$input['password'] = $request->password;
    	$this->userService->login($input);
    	return redirect()->back();
    }

    public function regist(RegistCustomerRequest $request)
    {
    	$input['fullname'] = $request->fullname;
        $input['username'] = $request->username;
    	$input['password'] = $request->password;
    	$this->userService->regist($input);
    	return redirect()->back();
    }

    public function logout()
    {
    	$this->userService->logout();
    	return redirect()->back();
    }

    public function edit()
    {
        $view = 'Site.EditProfileCustomer';
        return view($view);
    }
}
