<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Services\PropertiesService;

class PropertiesController extends Controller
{
    private $propertiesService;

    public function __construct(
        PropertiesService $PropertiesService
    )
    {
        $this->propertiesService = $PropertiesService;
    }

    public function getDetail ($id) {
        $data = $this->propertiesService->getDetail($id);
        $conditions = [
            'property_basic_info_type' => $data[0]['property_basic_info_type'],
            'property_basic_info_city' => $data[0]['property_basic_info_city'],
        ];
        $data_property_suggestions = $this->propertiesService->getInfoPropertySuggestion($conditions);
        return view('component.PropertiesDetail',['data' => $data[0],'property_suggestions'=>$data_property_suggestions]);
    }
}
