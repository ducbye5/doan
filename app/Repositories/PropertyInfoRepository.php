<?php

namespace App\Repositories;

use App\Models\PropertyInfo;
use App\Repositories\Interfaces\PropertyInfoRepositoryInterface;
use DB;

class PropertyInfoRepository implements PropertyInfoRepositoryInterface
{
	private $model;
	public function __construct(
        PropertyInfo $PropertyInfo
	){
		$this->model = $PropertyInfo;
	}

	public function create(array $data = [])
	{
		$result = false;
		if($data != []){
			$result = $this->model->insert($data);
		}
		return $result;
	}

	public function createAndGetID(array $input = [])
	{
		$result = false;
		if($input != []){
			$result = $this->model->insertGetId($input);
		}
		return $result;
	}

	public function get_top_latest_record($type = '',$number = 0){
	    $colums = [
            'id',
            'property_picture',
            'property_basic_info_title',
            'property_basic_info_type',
            'property_basic_info_type_of_properties',
            'property_basic_info_acreage',
            'property_basic_info_price',
            'property_basic_info_unit',
            'property_basic_info_total_price',
            'property_basic_info_address',
            'property_other_info_number_bedrooms',
            'property_other_info_number_toilets',
            'property_calendar_type_of_cost'
        ];
		$query = $this->model
					->select($colums)
					->where('property_calendar_type_of_cost',$type)
					->orderBy('id','desc');
		if($type = 'Normal'){
			$result = $query->get();
		}else{
			$result = $query->limit($number)
				->get();
		}
		return $result;
	}

	public function get_all_record($record_number = 0,$array_order_by = ['id' => 'desc'],$type = null){
	    $colums = [
	        'id',
            'property_picture',
            'property_basic_info_title',
            'property_basic_info_type',
            'property_basic_info_type_of_properties',
            'property_basic_info_acreage',
            'property_basic_info_price',
            'property_basic_info_unit',
            'property_basic_info_total_price',
            'property_basic_info_address',
            'property_other_info_number_bedrooms',
            'property_other_info_number_toilets',
            'property_calendar_type_of_cost'
        ];
		$array_key = array_keys($array_order_by);
		$array_value = array_values($array_order_by);
		$query = $this->model
					->select($colums);
		if(!empty($type)){
			$query = $query->where('property_basic_info_type',$type);
		}
		for($i = 0 ;$i < count($array_order_by);$i++){
			$query->orderBy($array_key[$i],$array_value[$i]);
		}
		if($record_number == 0){
			$result = $query->get();
		}else{
			$result = $query->paginate($record_number);
		}
		return $result;
	}

	public function search_property($record_number = 0,$data = []){
		$query = $this->model
					->select('*');
		if($data == []){
			$query = $query->orderBy('name','desc');
		}else{
			if(!empty($data['type'])){

				$query = $query->orderBy('prioritize_01','desc')
							->where('property_basic_info_type',$data['type']);
			}
			if(!empty($data['type_of_property'])){
				$query = $query->where('property_basic_info_type_of_properties',$data['type_of_property']);
			}
			if(!empty($data['address'])){
				$query = $query->where('property_basic_info_address','like','%'.$data['address'].'%');
			}
			if(!empty($data['area'])){
				$query = $query->where('property_basic_info_acreage','>=',$data['area']);
			}
			if(!empty($data['bedrooms'])){
				if($data['bedrooms'] == '5plus'){
					$query = $query->where('property_other_info_number_bedrooms','>',5);
				}else{
					$query = $query->where('property_other_info_number_bedrooms',$data['bedrooms']);
				}
			}
			if(!empty($data['bathrooms'])){
				if($data['bathrooms'] == '5plus'){
					$query = $query->where('property_other_info_number_toilets', '>', 5);
				}else{
					$query = $query->where('property_other_info_number_toilets',$data['bathrooms']);
				}
			}
			if(!empty($data['minprice'])){
				$query = $query->where('property_basic_info_total_price','>=',$data['minprice']);
			}
			if(!empty($data['maxprice'])){
					$query = $query->where('property_basic_info_total_price','<=',$data['maxprice']);
			}
			if(!empty($data['sortby']) && !empty($data['orderBy'])){
				$query = $query->orderBy($data['sortby'],$data['orderBy']);
			}
		}
		if($record_number == 0){
			$result = $query->get();
		}else{
			$result = $query->paginate($record_number);
		}
		return $result;
	}


	public function getListByCondition(array $conditions,$id = null){
		$array_key = array_keys($conditions);
		$array_value = array_values($conditions);
		$query = $this->model
					->select(
						'property_infos.id',
						'property_picture',
						'property_basic_info_title',
						'property_basic_info_type',
						'property_basic_info_total_price',
						'property_basic_info_acreage',
						'property_basic_info_address',
						'property_other_info_number_bedrooms',
						'property_other_info_number_toilets'
					);
		for($i = 0 ;$i < count($conditions);$i++){
			$query->where($array_key[$i],$array_value[$i]);
		}
		if(!empty($id)){
			$query->where('id','!=',$id);
		}
		$result = $query->orderBy('id','desc')->limit(10)->get();
		return $result;
	}

	public function getDetail($id = null) {
        $query = $this->model
                    ->select('*')
                    ->where('id',$id);
         $result = $query->get()->toArray();
         return $result;
    }
}
