<?php

namespace App\Repositories\Interfaces;

interface InformationOfCompanyRepositoryInterface
{
	public function create();

    public function get();

    public function update();

    public function delete();

    public function getInformationOfCompany(array $column,array $condition);
}