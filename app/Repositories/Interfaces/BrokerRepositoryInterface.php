<?php

namespace App\Repositories\Interfaces;

interface BrokerRepositoryInterface
{
	public function create();

    public function get();

    public function update();

    public function delete();

    public function getInformationOfBroker(array $column,array $condition,array $sort);
}