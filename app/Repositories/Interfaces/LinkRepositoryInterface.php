<?php

namespace App\Repositories\Interfaces;

interface LinkRepositoryInterface
{
	public function create();

    public function get();

    public function update();

    public function delete();

    public function createAndGetID(array $data);

    public function findById(int $id,array $column);
}
