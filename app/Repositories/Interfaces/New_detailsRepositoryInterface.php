<?php

namespace App\Repository\Interfaces;

interface New_detailsRepositoryInterface
{
	public function createAndGetID(array $input);
}