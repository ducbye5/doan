<?php

namespace App\Repositories\Interfaces;

interface UserRepositoryInterface
{
	public function create(array $input);
	public function get(array $column);
	public function update(int $id, array $input);
	public function delete(int $id);
	public function findByUsername(string $username, array $column);
	public function findByID(int $id, array $column);
}
