<?php

namespace App\Repositories\Interfaces;

interface PropertyHistoryRepositoryInterface
{
	public function create(array $data);
}
