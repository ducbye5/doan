<?php

namespace App\Repositories;

use App\Models\PropertyHistory;
use App\Repositories\Interfaces\PropertyHistoryRepositoryInterface;

class PropertyHistoryRepository implements PropertyHistoryRepositoryInterface
{
	private $model;
	public function __construct(
        PropertyHistory $PropertyHistory
	){
		$this->model = $PropertyHistory;
	}

	public function create(array $data = [])
	{
		$result = false;
		if($data != []){
			$result = $this->model->insert($data);
		}
		return $result;
	}

}
