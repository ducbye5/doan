<?php

namespace App\Repositories;
use App\Repositories\Interfaces\UserRepositoryInterface;
use App\Models\User;

class UserRepository implements UserRepositoryInterface
{
	private $model;
	public function __construct(
		User $User
	){
		$this->model = $User;
	}

	public function create(array $input = []) {
		$result = false;
		if($input != []){
			$result = $this->model->insert($input);
		}
		return $result;
	}

	public function get(array $column = ['*']) {
		$result = $this->model
		               ->select($column)
		               ->get();
		return $result;
	}

	public function update(int $id = null,array $input = []) {
		$result = $this->model
		               ->where('id',$id)
		               ->update($input);
		return $result;
	}

	public function delete(int $id = null) {
		$result = $this->model
		               ->where('id',$id)
		               ->delete();
		return $result;
	}

	public function findByUsername(string $username = null,array $column = ['*'])
	{
		$result = $this->model
		               ->select($column)
		               ->where('username',$username)
		               ->get();
		return $result;
	}

	public function findByID(int $id = null,array $column = ['*'])
	{
		$result = $this->model
		               ->select($column)
		               ->where('id',$id)
		               ->get();
		return $result;
	}
}
