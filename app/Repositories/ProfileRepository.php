<?php

namespace App\Repositories;
use App\Repositories\Interfaces\ProfileRepositoryInterface;
use App\Models\Profile;
class ProfileRepository implements ProfileRepositoryInterface
{
    private $model;
    public function  __construct(
        Profile $Profile
    ) {
        $this->model = $Profile;
    }
    public function create() {

    }

    public function get() {

    }

    public function update() {

    }

    public function delete() {

    }

    public function createAndGetID(array $data) {
        $result = $this->model->insertGetId($data);
        return $result;
    }

    public function findById(int $id,array $column = ['*']) {
        $result = $this->model
            ->select($column)
            ->where('id',$id)
            ->get();
        return $result;
    }
}
