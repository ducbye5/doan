<?php

namespace App\Repositories;
use App\Repositories\Interfaces\LinkRepositoryInterface;
use App\Models\Link;
class LinkRepository implements LinkRepositoryInterface
{
    private $model;
    public function  __construct(
        Link $Link
    ) {
        $this->model = $Link;
    }
    public function create() {

    }

    public function get() {

    }

    public function update() {

    }

    public function delete() {

    }

    public function createAndGetID(array $data) {
        $result = $this->model->insertGetId($data);
        return $result;
    }

    public function findById(int $id,array $column = ['*']) {
        $result = $this->model
                    ->select($column)
                    ->where('id',$id)
                    ->get();
        return $result;
    }
}
