<?php

namespace App\Repositories;
use App\Repositories\Interfaces\BrokerRepositoryInterface;
use App\Models\Broker;

class BrokerRepository implements BrokerRepositoryInterface
{

    private $model;

    public function __construct(Broker $Broker)
    {
        $this->model = $Broker;
    }

    public function create() {

    }

    public function get() {
        
    }

    public function update() {

    }

    public function delete() {

    }

    public function getInformationOfBroker(array $column = ['*'],array $condition = [],array $sort = []) {
        $query = $this->model
                    ->select($column)
                    ->join('information_of_companies',function($join){
                        $join->on('company_id','=','information_of_companies.id');
                    });
        if($condition != []){
            $array_key = array_keys($condition);
            $array_value = array_values($condition);
            for($i = 0 ; $i < count($condition); $i++){
                $query = $query->where($array_key[$i],$array_value[$i]);
            }
        }
        if($sort != []){
            $array_key = array_keys($sort);
            $array_value = array_values($sort);
            for($i = 0 ; $i < count($sort); $i++){
                $query = $query->orderBy($array_key[$i],$array_value[$i]);
            }
        }
        $result = $query->get()->toArray();
        return $result;
    }
}