<?php

namespace App\Repositories;
use App\Repositories\Interfaces\InformationOfCompanyRepositoryInterface;
use App\Models\InformationOfCompany;

class InformationOfCompanyRepository implements InformationOfCompanyRepositoryInterface
{

    private $model;

    public function __construct(InformationOfCompany $InformationOfCompany)
    {
        $this->model = $InformationOfCompany;
    }

    public function create() {

    }

    public function get() {

    }

    public function update() {

    }

    public function delete() {

    }

    public function getInformationOfCompany(array $column = ['*'],array $condition = []) {
        $query = $this->model
                    ->select($column)
                    ->join('links',function($join){
                        $join->on('links_id','=','links.id');
                    });
        if($condition != []){
            $array_key = array_keys($condition);
            $array_value = array_values($condition);
            for($i = 0 ; $i < count($condition); $i++){
                $query = $query->where($array_key[$i],$array_value[$i]);
            }
        }
        $result = $query->get()->toArray();
        return $result;
    }
}
