<?php

namespace App\Services;
use App\Repositories\Interfaces\PropertyInfoRepositoryInterface;
use App\Services\HelpService;


class PropertiesService
{

    private $propertyInfoRepository;
    private $helpService;

    public function __construct(
        PropertyInfoRepositoryInterface $PropertyInfoRepository,
        HelpService $HelpService
    )
    {
        $this->propertyInfoRepository = $PropertyInfoRepository;
        $this->helpService = $HelpService;
    }

    public function getDetail($id) {
        $data = $this->propertyInfoRepository->getDetail($id);
        return $data;
    }

    public function getInfoPropertySuggestion($conditions) {
        $data = $this->propertyInfoRepository->getListByCondition($conditions);
        return $data;
    }
}
