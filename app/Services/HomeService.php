<?php

namespace App\Services;

use App\Repositories\Interfaces\InformationOfCompanyRepositoryInterface;
use App\Repositories\Interfaces\BrokerRepositoryInterface;
use App\Repositories\Interfaces\PropertyInfoRepositoryInterface;
use App\Services\HelpService;

class HomeService
{
    private $informationOfCompanyRepository;
    private $brokerRepository;
    private $propertyInfoRepository;
    private $helpService;

    public function __construct(
        InformationOfCompanyRepositoryInterface $InformationOfCompanyRepository,
        BrokerRepositoryInterface $BrokerRepository,
        PropertyInfoRepositoryInterface $PropertyInfoRepository,
        HelpService $HelpService
    ){
        $this->informationOfCompanyRepository = $InformationOfCompanyRepository;
        $this->brokerRepository = $BrokerRepository;
        $this->propertyInfoRepository = $PropertyInfoRepository;
        $this->helpService = $HelpService;
    }

    public function create() {

    }

    public function get() {

    }

    public function update() {

    }

    public function delete() {

    }

    public function index() {
        $data_slider = (object)[
            [
                'id' => 1,
                'property_picture' => 'slider1.jpg',
                'property_basic_info_unit'=>'Negotiate',
                'property_basic_info_total_price' => 20000,
                'property_basic_info_title' => 'rent house',
                'property_basic_info_address' => 'Gia Lâm - Hà nội - Viet Nam',
                'property_basic_info_acreage' => 100
            ],
            [
                'id' => 2,
                'property_picture' => 'slider2.jpg',
                'property_basic_info_unit'=>'Negotiate',
                'property_basic_info_total_price' => 20000,
                'property_basic_info_title' => 'rent house',
                'property_basic_info_address' => 'Gia Lâm - Hà nội - Viet Nam',
                'property_basic_info_acreage' => 100
            ],[
                'id' => 3,
                'property_picture' => 'slider3.jpg',
                'property_basic_info_unit'=>'',
                'property_basic_info_total_price' => 20000,
                'property_basic_info_title' => 'rent house',
                'property_basic_info_address' => 'Gia Lâm - Hà nội - Viet Nam',
                'property_basic_info_acreage' => 100
            ],
        ];
        $data_property_special = $this->propertyInfoRepository->get_top_latest_record('Special',5);
        $data_property_vip = $this->propertyInfoRepository->get_top_latest_record('Vip',5);
        $data_property = $this->propertyInfoRepository->get_all_record(9);
        $result = [
            'slider' => $data_slider,
            'property_special' => $data_property_special,
            'property_vip' => $data_property_vip,
            'property' => $data_property,
        ];
        return $result;
    }

    public function getInformationOfCompany() {
        $column_link_company = ['facebook','twitter','linkedin','skype','slack'];
        $condition_link_company = ['office_priority' => 1];
        $data_link_company = $this->informationOfCompanyRepository->getInformationOfCompany($column_link_company,$condition_link_company);
        $column_information_company = ['office_branch','office_address','office_telephone','fax','email'];
        $data_information_company = $this->informationOfCompanyRepository->getInformationOfCompany($column_information_company);
        $column_broker = ['brokers.id','broker_name','broker_avatar','office_address','broker_description'];
        $sort_broker = ['broker_level' => 'desc'];
        $data_top_broker = $this->brokerRepository->getInformationOfBroker($column_broker,[],$sort_broker);
        $data = [
            'data_main' => $data_link_company[0],
            'data_info' => $data_information_company,
            'data_broker' => $data_top_broker,
        ];
        return $data;
    }
}
