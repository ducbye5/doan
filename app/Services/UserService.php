<?php

namespace App\Services;
use App\Repositories\Interfaces\UserRepositoryInterface;
use App\Repositories\Interfaces\ProfileRepositoryInterface;
use App\Repositories\Interfaces\LinkRepositoryInterface;
use DB;
class UserService
{
    private $userRepository;
    private $profileRepository;
    private $linkRepository;
    public function __construct(
        UserRepositoryInterface $UserRepository,
        ProfileRepositoryInterface $ProfileRepository,
        LinkRepositoryInterface $LinkRepository
    ){
        $this->userRepository = $UserRepository;
        $this->profileRepository = $ProfileRepository;
        $this->linkRepository = $LinkRepository;
    }

    public function login($input)
    {
        $status = false;
        $record_user = $this->userRepository->findByUsername($input['username'])->toArray();
        if(count($record_user) != 0 && \Hash::check($input['password'],$record_user[0]['password'])){
            $data_link = $this->linkRepository->findById($record_user[0]['links_id'])->toArray();
            $data_profile = $this->profileRepository->findById($record_user[0]['profiles_id'])->toArray();
            $data_user = array_merge($record_user[0],$data_link[0],$data_profile[0]);
            unset($data_user['password']);
            \Session::put(['user' => $data_user]);
            \Session::flash('message',__('Message.login_success'));
            $status = true;
        }else{
            \Session::flash('message',__('Message.login_false'));
        }
        return $status;
    }

    public function regist($input)
    {
        DB::beginTransaction();
        try{
            $result = $this->doRegistCustomer($input);
            if($result){
                DB::commit();
                $status = $this->login($result);
            }else{
                DB::rollback();
                \Session::flash('message',__('Message.create_account_false'));
                $status = false;
            }
        }catch(\Exception $e){
            \Log::error($e->getMessage());
            DB::rollback();
            \Session::flash('message',__('Message.create_account_false'));
            $status = false;
        }
        return $status;
    }

    public function logout()
    {
        if(\Session::has('user')){
            \Session::forget('user');
        }
        return false;
    }

    private function doRegistCustomer($input)
    {
        $data_input['updated_at'] = now();
        $data_input['created_at'] = now();
        $links_id = $this->linkRepository->createAndGetId($data_input);
        $data_input['profile_fullname'] = $input['fullname'];
        $profiles_id = $this->profileRepository->createAndGetId($data_input);
        $data_input['links_id'] = $links_id;
        $data_input['profiles_id'] = $profiles_id;
        $data_input['username'] = $input['username'];
        $data_input['password'] = \Hash::make($input['password']);
        unset($data_input['profile_fullname']);
        unset($input['fullname']);
        $check_insert_data = $this->userRepository->create($data_input);
        if($check_insert_data){
            return $input;
        }else{
            return false;
        }

    }
}
