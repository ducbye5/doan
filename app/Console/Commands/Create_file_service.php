<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class Create_file_service extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:service {filename}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'create file service';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $pathFolder = './app/Services';
        if (!file_exists($pathFolder)) {
            mkdir($pathFolder, 0777, true);
        }
        $fileName = $this->argument('filename');
        $fullFileName = $fileName.'Service.php';
        $fileService = fopen("./app/Services/".$fullFileName, "x+");
        $content = '<?php

namespace App\Services;

class '.$fileName.'Service'.'
{
    public function __construct()
    {
        
    }

    public function create() {

    }

    public function get() {

    }

    public function update() {

    }

    public function delete() {

    }
}';
        fwrite($fileService, $content);
        fclose($fileService);
    }
}
