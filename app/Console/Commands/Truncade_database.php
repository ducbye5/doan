<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class Truncade_database extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'database:truncade';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'delete all record in database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $list_table = [
            'brokers',
            'information_of_companies',
            'users',
            'profiles',
            'links'
        ];
        foreach($list_table as $table) {
            $query = "TRUNCATE TABLE ".$table." RESTART IDENTITY CASCADE;";
            \DB::statement($query);
        }
    }
}
