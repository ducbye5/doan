<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class Create_file_model extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:model {filename}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $pathFolder = './app/Models';
        if (!file_exists($pathFolder)) {
            mkdir($pathFolder, 0777, true);
        }
        $fileName = $this->argument('filename');
        $fullFileName = $fileName.'.php';
        $fileService = fopen("./app/Models/".$fullFileName, "x+");
        $content = '<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class '.$fileName.' extends Model'.'
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
         
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        
    ];   
}';
        fwrite($fileService, $content);
        fclose($fileService);
    }
}
