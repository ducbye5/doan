<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class Create_file_repository extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:repository {filename}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'create repository';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $pathFolderRoot = './app/Repositories';
        if (!file_exists($pathFolderRoot)) {
            mkdir($pathFolderRoot, 0777, true);
        }
        $pathFolderInterface = './app/Repositories/Interfaces';
        if (!file_exists($pathFolderInterface)) {
            mkdir($pathFolderInterface, 0777, true);
        }
        $fileName = $this->argument('filename');
        $fileNameRepository = $fileName.'Repository.php';
        $fileNameInterface = $fileName.'RepositoryInterface.php';

        $fileRepository = fopen("./app/Repositories/".$fileNameRepository, "x+");
        $contentFileRepository = '<?php

namespace App\Repositories;
use App\Repositories\Interfaces'.'\\'.$fileName.'RepositoryInterface;

class '.$fileName.'Repository implements '.$fileName.'RepositoryInterface
{
    public function __construct()
    {
        
    }

    public function create() {

    }

    public function get() {

    }

    public function update() {

    }

    public function delete() {

    }
}';
        fwrite($fileRepository, $contentFileRepository);
        fclose($fileRepository);

        $fileInterface = fopen("./app/Repositories/Interfaces/".$fileNameInterface, "x+");
        $contentFileInterface = '<?php

namespace App\Repositories\Interfaces;

interface '.$fileName.'RepositoryInterface
{
    public function create();

    public function get();

    public function update();

    public function delete();
}';
        fwrite($fileInterface, $contentFileInterface);
        fclose($fileInterface);
    }
}
