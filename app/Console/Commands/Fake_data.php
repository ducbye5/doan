<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class Fake_data extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fake:data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'fake data in database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        factory(\App\Models\Broker::class,3)->create();
        factory(\App\Models\PropertyHistory::class,1)->create();
    }
}
