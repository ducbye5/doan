<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

//Auth::routes();

Route::group(array('middleware'=>['site']),function(){
	Route::group(array('middleware'=>[]),function(){
		Route::get('/',function(){
			return redirect()->route('home.index');
		});

		Route::post('logincustomer',[
			'uses' => 'Site\CustomerController@login',
			'as' => 'customer.login'
		]);

		Route::post('registcustomer',[
			'uses' => 'Site\CustomerController@regist',
			'as' => 'customer.regist'
		]);

		Route::get('logoutcustomer',[
			'uses' => 'Site\CustomerController@logout',
			'as' => 'customer.logout'
		]);

		//index
		Route::get('/index',[
				'uses'=>'HomeController@index',
				'as'=>'home.index'
			]);
		Route::get('/index-map',[
				'uses'=>'HomeController@indexMap',
				'as'=>'home.index-map'
			]);
		Route::get('/search', [
				'uses'=>'HomeController@searchProperties',
				'as'=>'home.searchProperties'
			]);

		//customers

		Route::group(array('prefix'=>'customer'),function(){
			//edit profile
			Route::get('editprofile',[
				'uses' => 'Site\CustomerController@edit',
				'as' => 'customer.editprofile'
			]);
		});

		//register Properties
		Route::get('/register_properties',[
				'uses'=>'Site\RegistPropertiesController@index',
				'as'=>'register_properties.index'
			]);
		Route::post('/register_properties',[
				'uses'=>'Site\RegistPropertiesController@regist',
				'as'=>'register_properties.regist'
			]);
		//buy coint
		Route::get('/coint',[
				'uses'=>'Site\CointController@index',
				'as'=>'coint.index'
			]);
		//properties
		Route::group(array('prefix'=>'properties'),function(){
			Route::get('/{type}', [
				'uses'=>'Site\PropertiesForSellController@index',
				'as'=>'properties.index'
			]);
			Route::post('/{type}', [
				'uses'=>'Site\PropertiesForSellController@searchProperties',
				'as'=>'properties.searchProperties'
			]);
			Route::get('/detail/{id}', [
				'uses'=>'Site\PropertiesController@getDetail',
				'as'=>'properties.detail'
			]);
		});
	});

	//agent
	Route::get('/agent/{id}',[
			'uses'=>'Site\AgentController@index',
			'as'=>'agent.index'
		]);

	//blog
	Route::get('/blog',[
			'uses'=>'Site\BlogController@index',
			'as'=>'blog.index'
		]);

	//contact us
	Route::get('/contact_us',[
			'uses'=>'Site\ContactUsController@index',
			'as'=>'contact_us.index'
		]);

	//page
	Route::group(array('prefix'=>'page'),function(){
		Route::get('/about_us',[
			'uses'=>'Site\AboutUsController@index',
			'as'=>'about_us.index'
		]);
		Route::get('/faq',[
			'uses'=>'Site\FaqController@index',
			'as'=>'faq.index'
		]);
	});
});

Route::get('/home', 'HomeController@index')->name('home');

