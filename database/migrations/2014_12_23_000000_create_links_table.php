<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLinksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('links', function (Blueprint $table) {
            $table->increments('id');
            $table->string('facebook',500)->nullable()->default('https://www.facebook.com/');
            $table->string('twitter',500)->nullable()->default('https://www.twitter.com/');
            $table->string('linkedin',500)->nullable()->default('https://www.linkedin.com/');
            $table->string('pinterest',500)->nullable()->default('https://www.pinterest.com/');
            $table->string('skype',500)->nullable()->default('https://www.skype.com/');
            $table->string('slack',500)->nullable()->default('https://www.slack.com/');
            $table->string('email',500)->nullable();
            $table->string('fax',500)->nullable()->default('123456789');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('links');
    }
}
