<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertyInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_infos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('property_picture');
            $table->string('property_basic_info_title',100);
            $table->string('property_basic_info_type',8);
            $table->string('property_basic_info_type_of_properties',17);
            $table->string('property_basic_info_acreage');
            $table->string('property_basic_info_price');
            $table->string('property_basic_info_unit');
            $table->integer('property_basic_info_total_price');
            $table->string('property_basic_info_address');
            $table->text('property_description_info',3000);
            $table->integer('property_other_info_number_bedrooms')->nullable();
            $table->integer('property_other_info_number_toilets')->nullable();
            $table->string('property_calendar_type_of_cost');
            $table->date('property_calendar_time_start');
            $table->date('property_calendar_time_finish');
            $table->integer('prioritize_01')->nullable();
            $table->string('property_basic_info_country')->nullable();
            $table->string('property_basic_info_city')->nullable();
            $table->string('property_basic_info_district')->nullable();
            $table->string('property_basic_info_ward')->nullable();
            $table->string('property_basic_info_street')->nullable();
            $table->string('property_basic_info_project')->nullable();
            $table->string('property_other_info_facade')->nullable();
            $table->string('property_other_info_direction_house')->nullable();
            $table->string('property_other_info_direction_balcony')->nullable();
            $table->string('property_other_info_number_floors')->nullable();
            $table->string('property_contact_info_mobile_phone');
            $table->string('property_contact_info_email')->nullable();
            $table->string('property_contact_info_name')->nullable();
            $table->string('property_contact_info_address')->nullable();
//            $table->integer('property_detail_id')->unsigned();
//            $table->foreign('property_detail_id')->references('id')->on('property_details')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('property_infos');
    }
}
