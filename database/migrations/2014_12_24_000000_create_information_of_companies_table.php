<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInformationOfCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('information_of_companies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('office_branch')->nullable();
            $table->string('office_address')->nullable();
            $table->string('office_telephone')->nullable();
            $table->integer('office_priority')->nullable();
            $table->string('ceo_name')->nullable();
            $table->string('ceo_avatar')->nullable();
            $table->string('ceo_description',1000)->nullable();
            $table->integer('links_id')->unsigned();
            $table->foreign('links_id')->references('id')->on('links')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('information_of_companies');
    }
}
