<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Models\User::class, function (Faker $faker) {
    return [
        'username' => 'ducbye12',
        'password' => '$2y$10$ktDlHdz5Y3Cq0tcNIWzUPebg9gzLQSuypaWt1OH3hBGfA9j.TTC0y',
        'coint' => 1000,
        'profiles_id' => factory(\App\Models\Profile::class),
        'links_id' => factory(\App\Models\Link::class),
        'created_at' => '2019-01-04 07:37:06',
        'updated_at' => '2019-01-04 07:37:06',
    ];
});
