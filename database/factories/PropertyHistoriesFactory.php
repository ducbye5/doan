<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use Faker\Generator as Faker;

$factory->define(App\Models\PropertyHistory::class, function (Faker $faker) {
    return [
    	'user_id' => factory(App\Models\User::class),
    	'property_id' => factory(App\Models\PropertyInfo::class),
    	'property_fees' => 15,
        'created_at' => '2019-01-04 07:37:06',
        'updated_at' => '2019-01-04 07:37:06',
    ];
});
