<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use Faker\Generator as Faker;


$factory->define(App\Models\PropertyInfo::class, function (Faker $faker) {
    $arr_typeofcost = [
        'Vip',
        'Normal',
        'Special',
        'Goodwill'
    ];

    $images = [
        'property-1.jpg',
        'property-2.jpg',
        'property-3.jpg',
        'property-4.jpg',
        'property-5.jpg',
        'property-6.jpg'
    ];

    return [
    	'property_picture' => $images[array_rand($images)],
    	'property_basic_info_title' => 'rent house',
    	'property_basic_info_type' => 'rent',
    	'property_basic_info_type_of_properties' => 'Dwelling_House',
    	'property_basic_info_acreage' => 100,
    	'property_basic_info_price' => 20000,
    	'property_basic_info_unit' => 'Dollar',
        'property_basic_info_total_price' => 20000,
    	'property_basic_info_address' => 'Gia Lâm - Hà nội - Viet Nam',
    	'property_description_info' => '1234567899',
    	'property_other_info_number_bedrooms' => 3,
    	'property_other_info_number_toilets' => 2,
    	'property_calendar_type_of_cost' => $arr_typeofcost[array_rand($arr_typeofcost)],
    	'property_calendar_time_start' => '2019-01-04',
    	'property_calendar_time_finish' => '2019-01-11',
        'prioritize_01' => 2,
        'property_basic_info_country' => 'Viet Nam',
        'property_basic_info_city' => 'Hà nội',
        'property_basic_info_district' => 'Gia Lâm',
        'property_basic_info_ward' => null,
        'property_basic_info_street' => null,
        'property_basic_info_project' => null,
        'property_other_info_facade' => null,
        'property_other_info_direction_house' => null,
        'property_other_info_direction_balcony' => null,
        'property_other_info_number_floors' => null,
        'property_contact_info_mobile_phone' => 1234567897,
        'property_contact_info_email' => null,
        'property_contact_info_name' => null,
        'property_contact_info_address' => null,
        'created_at' => '2019-01-04 07:37:06',
        'updated_at' => '2019-01-04 07:37:06',
    ];
});
