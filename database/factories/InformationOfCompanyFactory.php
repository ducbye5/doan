<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\InformationOfCompany;
use Faker\Generator as Faker;

$factory->define(App\Models\InformationOfCompany::class, function (Faker $faker) {
    return [
        'office_branch' => 'Việt Nam',
        'office_address' => 'Hà Nội',
        'office_telephone' => '123456789',
        'office_priority' => 1,
        'ceo_name' => 'Nguyen Van B',
        'ceo_avatar' => 'team-1.jpg',
        'ceo_description' => 'abc',
        'links_id' => factory(\App\Models\Link::class),
        'created_at' => '2019-01-04 07:37:06',
        'updated_at' => '2019-01-04 07:37:06',
    ];
});
