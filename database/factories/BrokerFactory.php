<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Broker;
use Faker\Generator as Faker;

$factory->define(App\Models\Broker::class, function (Faker $faker) {
    return [
        'broker_name' => 'Nguyen Van A',
        'broker_avatar' => 'agent-1.jpg',
        'broker_telephone' => '123456789',
        'broker_description' => 'abc',
        'broker_level' => 1,
        'company_id' => factory(\App\Models\InformationOfCompany::class),
         'links_id' => factory(\App\Models\Link::class),
//        'links_id' => 1,
        'created_at' => '2019-01-04 07:37:06',
        'updated_at' => '2019-01-04 07:37:06',
    ];
});
