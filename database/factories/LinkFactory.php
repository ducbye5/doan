<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use Faker\Generator as Faker;

$factory->define(App\Models\Link::class, function (Faker $faker) {
    return [
        'facebook' => 'https://www.facebook.com/',
        'twitter' => 'https://www.twitter.com/',
        'linkedin' => 'https://www.linkedin.com/',
        'pinterest' => 'https://www.pinterest.com/',
        'skype' => 'https://www.skype.com/',
        'slack' => 'https://slack.com/',
        'email' => 'ducbye6@gmail.com',
        'fax' => '12345678',
        'created_at' => '2019-01-04 07:37:06',
        'updated_at' => '2019-01-04 07:37:06',
    ];
});
