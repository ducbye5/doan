const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js(['resources/js/site/ajax/search.js'],'public/js/site/ajax')
	.js(['resources/js/site/custom/custom.js'],'public/js/site/custom')
	.js(['resources/js/site/payment/payment.js'],'public/js/site/payment')
	.js(['resources/js/site/profile/profile.js'],'public/js/site/profile')
	.js(['resources/js/site/registerProperty/registerProperty.js'],'public/js/site/registerProperty')
	.js(['resources/js/site/site.js'],'public/js/site')
	.js('resources/js/app.js', 'public/js')
	.sass('resources/sass/app.scss', 'public/css');
