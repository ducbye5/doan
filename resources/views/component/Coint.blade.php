@extends('layouts.index')
@section('controller')
<div role="main" class="main pgl-bg-grey">

			<!-- Begin content with sidebar -->
			<div class="container">
				<div class="row">
					<div class="col-md-3 sidebar">
						@include('component.include.SlidebarProfileCustomer')
					</div>
					<div class="col-md-9 content">

						@include('component.include.BuyCoint')

					</div>

				</div>
			</div>
			<!-- End content with sidebar -->

</div>
@endsection
