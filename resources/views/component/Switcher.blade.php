
	@if(\Session::has('user'))
	<div id="style-switcher">
		<div id="toggle_button"> <a href="#"><i class="fa fa-user"></i></a> </div>
		<div id="style-switcher-menu">
			<h4 class="text-center">Options</h4>
			<div class="segment">
				<div><a href="{{ route('customer.editprofile') }}" class="btn btn-sm">Profile Manager</a></div>
				<div><a href="{{ route('register_properties.index') }}" class="btn btn-sm">Register Properties</a></div>
				<div><a href="{{ route('coint.index') }}" class="btn btn-sm">Buy Coint</a></div>
			</div>
			<div class="segment">
				<div id="reset"> <a href="{{ route('customer.logout') }}" class="btn btn-sm reset">Logout</a> </div>
			</div>
		</div>
	</div>
	@endif
