@extends('layouts.index')
@section('controller')
<div role="main" class="main pgl-bg-grey">

			<!-- Begin content with sidebar -->
			<div class="container">
				<div class="row">
					@if(\Session::has('user'))
					<div class="col-md-3 sidebar">
						@include('component.include.SlidebarProfileCustomer')
					</div>
					<div class="col-md-9 content">
						@include('component.include.RegistProperties')
					</div>
					@else
					<div class="col-md-10 col-md-offset-1 content">
						@include('component.include.RegistProperties')
					</div>
					@endif
				</div>
			</div>
			<!-- End content with sidebar -->

</div>
@endsection
