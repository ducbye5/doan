
<footer class="pgl-footer">
	<div class="container">
		<div class="pgl-upper-foot">
			<div class="row">
				<div class="col-sm-4">
					<h2>Contact detail</h2>
					@foreach($company['data_info'] as $address)
					<address>
						<i class="fa fa-map-marker"></i> Office : {{ $address['office_branch'] }}<br>
						<i class="fa fa-map-marker"></i> Address : {{ $address['office_address'] }}<br>
						<i class="fa fa-phone"></i> Mobile : {{ $address['office_telephone'] }}<br>
						<i class="fa fa-fax"></i> Fax : {{ $address['fax'] }}<br>
						<i class="fa fa-envelope-o"> Mail: {{ $address['email'] }}</i>
					</address>
					@endforeach
				</div>
				<div class="col-sm-2">
					<h2>Useful links</h2>
					<ul class="list-unstyled">
						<li><a href="#">Help and FAQs</a></li>
						<li><a href="#">Home Price</a></li>
						<li><a href="#">Market View</a></li>
						<li><a href="#">Free Credit Report</a></li>
						<li><a href="#">Terms and Conditions</a></li>
						<li><a href="#">Privacy Policy</a></li>
						<li><a href="#">Community Guidelines</a></li>
					</ul>
				</div>
				<div class="col-sm-2">
					<h2>Pages</h2>
					<ul class="list-unstyled">
						<li><a href="#">Font &amp; Color</a></li>
						<li><a href="#">Blogs</a></li>
						<li><a href="#">Contact Us</a></li>
						<li><a href="#">404 Page</a></li>
						<li><a href="#">Advanced Search</a></li>
						<li><a href="#">Property Custom Field</a></li>
						<li><a href="#">Google Map</a></li>
					</ul>
				</div>
				<div class="col-sm-4">
					<h2>Don’t miss out</h2>
					<p>In venenatis neque a eros laoreet eu placerat erat suscipit. Fusce cursus, erat ut scelerisque condimentum, quam odio ultrices leo.</p>
					<form class="form-inline pgl-form-newsletter" role="form" method="post" action="">
						<div class="form-group">
							<label class="sr-only" for="exampleInputEmail2">Email address</label>
							<input type="email" class="form-control" id="exampleInputEmail2" placeholder="Enter your email here">
						</div>
						<button type="submit" class="btn btn-submit"><i class="icons icon-submit"></i></button>
					</form>
				</div>
			</div>
		</div>
		<div class="pgl-copyrights">
			<p>Copyright © 2019 RealEstast. Designed by <a href="http://facebook.com/">Đỗ Minh Đức</a></p>
		</div>
	</div>
</footer>
