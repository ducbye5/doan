						<div class="row">
							<div class="col-xs-6 col-sm-3">
								<div class="form-group">
									<select name="search[country]" class="chosen-select" id="country">
										@component('component.include.option.country.Country',[])
										@endcomponent
									</select>
								</div>
							</div>
							<div class="col-xs-6 col-sm-3">
								<div class="form-group">
									<select name="search[city]" class="chosen-select" id="city">
										@component('component.include.option.city.VietNam',[])
										@endcomponent
									</select>
								</div>
							</div>
							<div class="col-xs-6 col-sm-3">
								<div class="form-group">
									<select name="search[district]" class="chosen-select" id="district">
										@component('component.include.option.district.VietNam',[])
										@endcomponent
									</select>
								</div>
							</div>
							<div class="col-xs-6 col-sm-3">
								<div class="form-group">
									<select name="search[project]" class="chosen-select" id="project">
										@component('component.include.option.project.VietNam',[])
										@endcomponent
									</select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-6 col-sm-3">
								<div class="form-group">
									<select name="search[area]" class="chosen-select" id="area">
										<option selected="selected" value="">Area From</option>
										<option value="50">50</option>
                                        <option value="100">100</option>
                                        <option value="150">150</option>
                                        <option value="200">200</option>
                                        <option value="250">250</option>
                                        <option value="300">300</option>
                                        <option value="350">350</option>
                                        <option value="400">400</option>
                                        <option value="400+">400+</option>
									</select>
								</div>
							</div>
							<div class="col-xs-6 col-sm-3">
								<div class="form-group">
									<select name="search[bedrooms]" class="chosen-select" id="bedrooms">
										<option selected="selected" value="">Bedrooms</option>
										<option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="5+">5+</option>
									</select>
								</div>
							</div>
							<div class="col-xs-6 col-sm-3">
								<div class="form-group">
									<select name="search[bathrooms]" class="chosen-select" id="bathrooms">
										<option selected="selected" value="">Bathrooms</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="5+">5+</option>
									</select>
								</div>
							</div>
							<div class="col-xs-6 col-sm-3">
								<div class="form-group">
									<div class="row pgl-narrow-row">
										<div class="col-xs-6">
											<select name="search[minprice]" class="chosen-select" id="minprice">
												<option selected="selected" value="">Price From</option>
                                                <option value="500">$500</option>
                                                <option value="1000">$1000</option>
                                                <option value="5000">$5000</option>
                                                <option value="10000">$10000</option>
                                                <option value="20000">$20000</option>
												<option value="50000">$50000</option>
                                                <option value="100000">$100000</option>
                                                <option value="200000">$200000</option>
                                                <option value="350000">$350000</option>
                                                <option value="500000">$500000</option>
                                                <option value="750000">$750000</option>
                                                <option value="1000000">$1000000</option>
											</select>
										</div>
										<div class="col-xs-6">
											<select name="search[maxprice]" class="chosen-select" id="maxprice">
												<option selected="selected" value="">Price To</option>
                                                <option value="1000">$1000</option>
                                                <option value="5000">$5000</option>
                                                <option value="10000">$10000</option>
                                                <option value="20000">$20000</option>
                                                <option value="50000">$50000</option>
                                                <option value="100000">$100000</option>
                                                <option value="200000">$200000</option>
                                                <option value="350000">$350000</option>
                                                <option value="500000">$500000</option>
                                                <option value="750000">$750000</option>
                                                <option value="1000000+">$1000000+</option>
											</select>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-6 col-sm-3">
								<div class="form-group">
									<button type="button" class="btn btn-block btn-primary" id="search">Find your home</button>
								</div>
							</div>
						</div>
