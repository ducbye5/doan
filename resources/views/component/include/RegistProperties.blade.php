
<section class="pgl-properties pgl-bg-grey">
	<div class="properties-full bg-color-white">
		<div class="row">
			<div class="col-md-12">
			  	<div class="">
			    	<form method="post" action="{{ route('register_properties.regist') }}" enctype="multipart/form-data" id="postnews">
						@csrf
						<div class="col-md-12" id="BasicInformation">
							<div class="row">
								@include('component.include.property.BasicInformation')
							</div>
						</div>
						<div class="col-md-12" id="DescriptionInformation">
							<div class="row">
								@include('component.include.property.DescriptionInformation')
							</div>
						</div>
						<div class="col-md-12" id="OthersInformation">
							<div class="row">
								@include('component.include.property.OthersInformation')
							</div>
						</div>
						<div class="col-md-12" id="Pictures">
							<div class="row">
								@include('component.include.property.Pictures')
							</div>
						</div>
						<div class="col-md-12" id="ContactInformation">
							<div class="row">
								@include('component.include.property.ContactInformation')
							</div>
						</div>
						<div class="col-md-12" id="PostCalendar">
							<div class="row">
								@include('component.include.property.PostCalendar')
							</div>
						</div>
{{--						<div class="col-md-12" id="Capcha">--}}
{{--							<div class="row">--}}
{{--								@include('component.include.property.Capcha')--}}
{{--							</div>--}}
{{--						</div>--}}
						<div class="col-md-12" id="Button">
							<div class="row">
								@include('component.include.property.Button')
							</div>
						</div>
						<div class="col-md-12" id="NotificationFooter">
							<div class="row">
								@include('component.include.property.NotificationFooter')
							</div>
						</div>
					</form>
			  	</div>

			</div>
		</div>
	</div>
</section>
<script type="text/javascript" src="{{ asset('js/site/registerProperty/registerProperty.js') }}"></script>
