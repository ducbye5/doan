@php
	$selected_desc = (!empty(old('search[orderby]')) && old('search[orderby]') == 'desc')?selected:'';
	$selected_asc = (!empty(old('search[orderby]')) && old('search[orderby]') == 'asc')?selected:'';
@endphp
<li><label>Order</label></li>
<li>
	<select name="search[orderby]" data-placeholder="Order" class="chosen-select" id="orderProperties">
		<option value="desc" {{ $selected_desc }}>Descending</option>
		<option value="asc" {{ $selected_asc }}>Ascending</option>
	</select>
</li>