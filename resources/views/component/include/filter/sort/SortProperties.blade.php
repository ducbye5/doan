@php
	$selected_name = (!empty(old('search[sortby]')) && old('search[sortby]') == 'name')?selected:'';
	$selected_area = (!empty(old('search[sortby]')) && old('search[sortby]') == 'area')?selected:'';
	$selected_date = (!empty(old('search[sortby]')) && old('search[sortby]') == 'date')?selected:'';
@endphp
<li><label>Sort by</label></li>
<li>
	<select name="search[sortby]" data-placeholder="Sort by" class="chosen-select" id="sortProperties">
		<option value="name" {{ $selected_name }}>Name</option>
		<option value="area" {{ $selected_area }}>Area</option>
		<option value="date" {{ $selected_date }}>Date</option>
	</select>
</li>