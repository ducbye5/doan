<ul class="list-inline list-sort pull-left">
	@component('component.include.filter.TypeOfProperties')
	@endcomponent
	@component('component.include.filter.sort.SortProperties')
	@endcomponent
	@component('component.include.filter.order.OrderProperties')
	@endcomponent
</ul>
