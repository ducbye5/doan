@php
	$selected_dwelling_house = (!empty(old('search[typeofproperties]')) 
							&& old('search[typeofproperties]') == 'Dwelling_House')?'selected':'';
	$selected_apartment = (!empty(old('search[typeofproperties]')) 
							&& old('search[typeofproperties]') == 'Apartment')?'selected':'';
	$selected_land = (!empty(old('search[typeofproperties]')) 
							&& old('search[typeofproperties]') == 'Land')?'selected':'';
	$selected_other_real_estate = (!empty(old('search[typeofproperties]')) 
							&& old('search[typeofproperties]') == 'Other_Real_Estate')?'selected':'';
@endphp
<li><label>Type Of Property</label></li>
<li>
	<select name="search[typeofproperties]" class="chosen-select" id="typeofproperties">
		<option value="">-- All Property --</option>
		<option value="Dwelling_House" {{ $selected_dwelling_house }}>Dwelling House</option>
		<option value="Apartment" {{ $selected_apartment }}>Apartment</option>
		<option value="Land" {{ $selected_land }}>Land</option>
		<option value="Other_Real_Estate" {{ $selected_other_real_estate }}>Other Real Estate</option>
	</select>
</li>