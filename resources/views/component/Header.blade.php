
<header>
	<div id="top">
		<div class="container">
			<!-- <p class="pull-left text-note hidden-xs"><i class="fa fa-phone"></i> Need Support? 1-800-666-8888</p> -->
			<ul class="nav nav-pills nav-top navbar-right">
				@if(\Session::has('user'))
				<li><a href="{{ route('customer.logout') }}" title="Logout" data-placement="bottom" data-toggle="tooltip" data-original-title="Logout"><i class="glyphicon glyphicon-log-out"></i></a></li>
				@else
				<li class="login"><a href="" data-toggle="modal" data-target="#login" title="Login"><i class="glyphicon glyphicon-log-in"></i></a></li>
				@endif
				<!-- <li><a href="" title="Email" data-placement="bottom" data-toggle="modal" data-original-title="Email" data-target="#report"><i class="fa fa-envelope-o"></i></a></li> -->
				<li><a href="{{ $company['data_main']['facebook'] }}" title="Facebook" data-placement="bottom" data-toggle="tooltip" data-original-title="Facebook" target="blank"><i class="fa fa-facebook"></i></a></li>
				<li><a href="{{ $company['data_main']['twitter'] }}" title="Twitter" data-placement="bottom" data-toggle="tooltip" data-original-title="Twitter" target="blank"><i class="fa fa-twitter"></i></a></li>
				<li><a href="{{ $company['data_main']['linkedin'] }}" title="Linkedin" data-placement="bottom" data-toggle="tooltip" data-original-title="Linkedin" target="blank"><i class="fa fa-linkedin"></i></a></li>
				<li><a href="{{ $company['data_main']['skype'] }}" title="Skype" data-placement="bottom" data-toggle="tooltip" data-original-title="Skype" target="blank"><i class="fa fa-skype"></i></a></li>
				<li><a href="{{ $company['data_main']['slack'] }}" title="Slack" data-placement="bottom" data-toggle="tooltip" data-original-title="Slack" target="blank"><i class="fa fa-slack"></i></a></li>
			</ul>
		</div>
	</div>
	<nav class="navbar navbar-default pgl-navbar-main" role="navigation">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
				<a class="logo" href="{{ route('home.index') }}"><img src="{{ asset('images/logo.png') }}" alt="Flatize"></a> </div>

			<div class="navbar-collapse collapse width">
				<ul class="nav navbar-nav pull-right">
					<li class="dropdown active"><a href="{{ route('home.index') }}" data-target="#" class="dropdown-toggle" data-toggle="dropdown">Home</a>
						<ul class="dropdown-menu">
							<li><a href="{{ route('home.index-map') }}">Home Map</a></li>
							<li><a href="{{ route('home.index') }}">Home Slider</a></li>
						</ul>
					</li>
					<li class="dropdown"><a href="{{ route('properties.index',['type' => 'sell']) }}" class="dropdown-toggle" data-toggle="dropdown">Sell and Rental</a>
						<ul class="dropdown-menu">
							<li><a href="{{ route('properties.index',['type' => 'sell']) }}">Properties for Sell</a></li>
							<li><a href="{{ route('properties.index',['type' => 'rental']) }}">Properties for Rental</a></li>
						</ul>
					</li>
					<li class="dropdown"><a href="{{ route('properties.index',['type' => 'purchase']) }}" class="dropdown-toggle" data-toggle="dropdown">Purchase and Rent</a>
						<ul class="dropdown-menu">
							<li><a href="{{ route('properties.index',['type' => 'purchase']) }}">Properties for Purchase</a></li>
							<li><a href="{{ route('properties.index',['type' => 'rent']) }}">Properties for Rent</a></li>
						</ul>
					</li>
					<li><a href="{{ route('register_properties.index') }}">Register properties articles</a></li>
	              	<li class="dropdown"><a href="{{ route('about_us.index') }}" class="dropdown-toggle" data-toggle="dropdown">Pages</a>
	                	<ul class="dropdown-menu">
	                  		<li><a href="{{ route('about_us.index') }}">About Us</a></li>
	                  		<li><a href="{{ route('faq.index') }}">FAQs</a></li>
	                  		<li><a href="{{ route('contact_us.index') }}">Contact Us</a></li>
	                  	</ul>
	              	</li>
	              	<li><a href="{{ route('blog.index') }}">Blog</a> </li>
	              	<li class="dropdown"><a href="" class="dropdown-toggle" data-toggle="dropdown">Others</a>
	                	<ul class="dropdown-menu">
	                  		<li><a href="">Utility Support</a></li>
	                  		<li><a href="">Helps & Guide</a></li>
	                	</ul>
	              	</li>
				</ul>
			</div>
		</div>
	</nav>
</header>

