<!doctype html>
<html lang="en">

<!-- Mirrored from pixelgeeklab.com/html/realestast/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 01 Sep 2018 16:01:11 GMT -->
<head>
	<meta charset="utf-8">
	<meta name="keywords" content="HTML5 Template" />
	<meta name="description" content="Flatize - Shop HTML5 Responsive Template">
	<meta name="author" content="pixelgeeklab.com">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>RealEstast</title>

	<!-- Google Fonts -->
	<link href='http://fonts.googleapis.com/css?family=Rochester' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Raleway:400,700' rel='stylesheet' type='text/css'>

	<!-- Bootstrap -->
	<link href="{{ asset('assets/site/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">

	<!-- Libs CSS -->
	<link href="{{ asset('assets/site/css/fonts/font-awesome/css/font-awesome.css') }}" rel="stylesheet">
	<link rel="stylesheet" href="{{ asset('assets/site/vendor/owl-carousel/owl.carousel.css') }}" media="screen">
	<link rel="stylesheet" href="{{ asset('assets/site/vendor/owl-carousel/owl.theme.css') }}" media="screen">
	<link rel="stylesheet" href="{{ asset('assets/site/vendor/flexslider/flexslider.css') }}" media="screen">
	<link rel="stylesheet" href="{{ asset('assets/site/vendor/chosen/chosen.css') }}" media="screen">

	<!-- Theme -->
	<link href="{{ asset('assets/site/css/theme-animate.css') }}" rel="stylesheet">
	<link href="{{ asset('assets/site/css/theme-elements.css') }}" rel="stylesheet">
	<link href="{{ asset('assets/site/css/theme-blog.css') }}" rel="stylesheet">
	<link href="{{ asset('assets/site/css/theme-map.css') }}" rel="stylesheet">
	<link href="{{ asset('assets/site/css/theme.css') }}" rel="stylesheet">

	<!-- Style Switcher-->
	<link rel="stylesheet" href="{{ asset('assets/site/style-switcher/css/style-switcher.css') }}">

	<!-- Theme Responsive-->
	<link href="{{ asset('assets/site/css/theme-responsive.css') }}" rel="stylesheet">
	<link href="{{ asset('css/site.css') }}" rel="stylesheet">
	<script src="{{ asset('assets/site/vendor/jquery.min.js') }}"></script>
</head>
<body>

	<div id="page">
		<!-- Begin header -->
		@component('component.Header')
		@endcomponent
		<!-- End header -->

		<!-- Begin Main -->
			@yield('controller')
		<!-- End Main -->

		<!-- Begin footer -->
		@component('component.Footer')
		@endcomponent
		<!-- End footer -->
	</div>



	<!-- Begin Style Switcher -->
	@component('component.Switcher',[])
	@endcomponent
	<!-- Begin Style Switcher -->
	@include('modal.LoginRegisterCustomerModal')
	@include('modal.ReportModal')
</body>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src='https://www.google.com/recaptcha/api.js?hl=vi'></script>

<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="{{ asset('assets/site/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/site/vendor/owl-carousel/owl.carousel.js') }}"></script>
<script src="{{ asset('assets/site/vendor/flexslider/jquery.flexslider-min.js') }}"></script>
<script src="{{ asset('assets/site/vendor/chosen/chosen.jquery.min.js') }}"></script>
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?v=3&amp;sensor=true"></script>
<script src="{{ asset('assets/site/vendor/gmap/gmap3.infobox.min.js') }}"></script>
<script src="{{ asset('assets/site/vendor/masonry/imagesloaded.pkgd.min.js') }}"></script>
<script src="{{ asset('assets/site/vendor/masonry/masonry.pkgd.min.js') }}"></script>

<!-- Theme Initializer -->
<script src="{{ asset('assets/site/js/theme.plugins.js') }}"></script>
<script src="{{ asset('assets/site/js/theme.js') }}"></script>

<!-- Style Switcher -->
<script type="text/javascript" src="{{ asset('assets/site/style-switcher/js/switcher.js') }}"></script>

<script src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script type="text/javascript" src="{{ asset('js/site/ajax/search.js') }}"></script>
<script src="{{ asset('js/site/site.js') }}"></script>
</html>
