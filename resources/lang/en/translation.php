<?php

return [
    //modal
    //LoginRegisterCustomerModal
    'LoginRegisterCustomerModal-login' => 'LOGIN',
    'LoginRegisterCustomerModal-username' => 'Username',
    'LoginRegisterCustomerModal-password' => 'Password',
    'LoginRegisterCustomerModal-register' => 'REGISTER',
    'LoginRegisterCustomerModal-fullname' => 'Fullname',
    'LoginRegisterCustomerModal-close' => 'Close',
];
