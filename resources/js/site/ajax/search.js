$(function(){


	$('#search').click(function() {

		var data_search = {
			country: $('#country').val(),
			city: $('#city').val(),
			district: $('#district').val(),
			project: $('#project').val(),
			area: $('#area').val(),
			bedrooms: $('#bedrooms').val(),
			bathrooms: $('#bathrooms').val()
		};

		// alert(data_search.country);

        $.ajaxSetup({ headers: { 'csrftoken' : '{{ csrf_token() }}' } });

		$.ajax({ 
	        type: 'GET', 
	        url: $('#formSearchIndex').attr('link'), 
	        data: {'search': data_search},
	        dataType: 'json', 
	        success:function(data) { 
	        	$('#list_properties').html(view_of_property(data));
	        },
	        error: function(data) {
	        	$('#list_properties').html('error');
	        }       
	    }); 
	});

	function view_of_property (data) {
		var output = 
	'<div class="col-xs-4 col-md-4 animation">'+
		'<div class="pgl-property">'+
			'<div class="property-thumb-info">'+
				'<div class="property-thumb-info-image">'+
					'<img alt="" class="img-responsive img-properties" src="">'+
					'<span class="property-thumb-info-label">'+
						'<span class="label price">10000</span>'+
						'<span class="label forrent">rent</span>'+
					'</span>'+
				'</div>'+
				'<div class="property-thumb-info-content">'+
					'<h3><a href="" class="{{ $text_color }}">abc</a></h3>'+
					'<address>ha noi</address>'+
				'</div>'+
				'<div class="amenities clearfix">'+
					'<ul class="pull-left">'+
						'<li><strong>Area: </strong>100<sup>m²</sup></li>'+
					'</ul>'+
					'<ul class="pull-right">'+
						'<li><i class="icons icon-bedroom"></i> 5</li>'+
						'<li><i class="icons icon-bathroom"></i> 5</li>'+
					'</ul>'+
				'</div>'+
			'</div>'+
		'</div>'+
	'</div>'

	return output;
	} 
})